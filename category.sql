-- MySQL dump 10.13  Distrib 8.0.27, for Linux (x86_64)
--
-- Host: localhost    Database: t1.01.2022
-- ------------------------------------------------------
-- Server version	8.0.27-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `category` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `parent_id` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` (`id`, `name`, `parent_id`) VALUES (1,'Главная',0),(2,'Стройматериалы',0),(3,'Хозтовары',0),(4,'Автозапчасти',0),(5,'Для наружных работ',2),(6,'Для внутренних работ',2),(7,'Моющие средства',3),(8,'Инструменты',3),(9,'Посуда',3),(10,'Прочее',3),(11,'Для легковых авто',4),(12,'Для грузовых авто',4),(13,'Для мотоциклов и квадроциклов',4),(14,'Кирпич',5),(15,'Пиломатериалы',5),(16,'Кровельные материалы',5),(17,'ЖБИ',5),(18,'Металлопрокат',5),(19,'Обои',6),(20,'Краски',6),(21,'Шпаклевка',6),(22,'Штукатурка',6),(23,'Плитка',6),(24,'Другое',6),(25,'Стиральные порошки',7),(26,'Средства для посуды',7),(27,'Мыло',7),(28,'Остальное',7),(29,'Молотки',8),(30,'Топоры',8),(31,'Отвертки',8),(32,'Пилы',8),(33,'Остальное',8),(34,'Тарелки',9),(35,'Стаканы',9),(36,'Ложки',9),(37,'Вилки',9),(38,'Светодиодные лампы',10),(39,'Скатерти',10),(40,'Средства от насекомых',10),(41,'Стартеры',11),(42,'Генераторы',11),(43,'Резина',11),(44,'Двигатель',11),(45,'Трансмиссия',11),(46,'Ходовая',11),(47,'Кузов',11),(48,'Стартеры',12),(49,'Генераторы',12),(50,'Резина',12),(51,'Двигатель',12),(52,'Трансмиссия',12),(53,'Ходовая',12),(54,'Кузов',12);
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-01-01 17:28:36
