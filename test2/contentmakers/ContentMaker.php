<?php
namespace Contentmakers;
    class ContentMaker {
    use \Conn\BasicSet;
        public static function getContent()
        {
            # Устанавливаем соединение с БД
            $pdo = \Conn\DatabaseManager::getConnection();
            $com = $pdo->prepare(\Conn\DatabaseManager::querySelect2());
            $com->execute();
            $result = $com->fetchAll(\PDO::FETCH_OBJ);

            //делаем из одномерного массива - двумерный, в котором 
            //первый ключ - parent_id
            $finalResult = array();
            foreach ($result as $value)
            {
                $finalResult[$value->parent_id][] = $value;
            }
            return $finalResult;
        }
#        public static function outTree($parent_id, $level) 
#        {
#            $categoryArr = array();
#            $categoryArr = self::getContent();
#            if (isset($categoryArr[$parent_id])) 
#            { //Если категория с таким parent_id существует
#                foreach ($categoryArr[$parent_id] as $value) 
#                { /**
#                    ** Выводим категорию 
#                    **  $level * 25 - отступ, $level - хранит текущий уровень вложености (0,1,2..)
#                    **/
#                    echo "<div style='margin-left:" . ($level * 25) . "px;'>" . $value->name . "</div>";
#                    $level++; //Увеличиваем уровень вложености
#                    
#                    //Рекурсивно вызываем этот же метод, но с новым $parent_id и $level
#                    self::outTree($value->id, $level);
#                    $level--; //Уменьшаем уровень вложености
#                }
#            }
#        }
        public static function outTree($parent_id, $level) 
        {
            $categoryArr = array();
            $categoryArr = self::getContent();
            if (isset($categoryArr[$parent_id])) 
            { //Если категория с таким parent_id существует
                foreach ($categoryArr[$parent_id] as $value) 
                { /**
                    ** Выводим категорию 
                    **  $level * 25 - отступ, $level - хранит текущий уровень вложености (0,1,2..)
                    **/
                    if ($level == 1)
                    {
                        echo "<div style='margin-left:" . ($level * 25) . "px;'>" . $value->name . "</div>";
                    }
                    elseif ($level == 2)
                    {
                        echo "<div class='subCategoryLevel2' style='margin-left:" . ($level * 25) . "px;'>" . $value->name . "</div>";
                    }
                    else
                    {
                        echo "<div class='subCategoryLevel3' style='margin-left:" . ($level * 25) . "px;'>" . $value->name . "</div>";
                    }
                    $level++; //Увеличиваем уровень вложености
                    
                    //Рекурсивно вызываем этот же метод, но с новым $parent_id и $level
                    self::outTree($value->id, $level);
                    $level--; //Уменьшаем уровень вложености
                }
            }
        }

    }
