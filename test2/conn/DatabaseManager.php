<?php ### Соединение с БД, запросы к БД

namespace Conn;
    class DatabaseManager {
        use BasicSet;
        private static $connection;
        private static $host;
        private static $user;
        private static $pass;
        private static $name;

        private final function __construct()
        {
            self::$host = $_ENV['DB_HOST'];
            self::$user = $_ENV['DB_USER'];
            self::$pass = $_ENV['DB_PASSWORD'];
            self::$name = $_ENV['DB_NAME'];
            self::$connection = new \PDO(self::$host /*. /*";port=37609;"*/ . ";" . self::$name, self::$user, self::$pass);
        }

        public static function getConnection()
        {
            try {
                self::getInstance();
                return self::$connection;
            } catch (PDOException $e) {
                print 'Не удалось соединиться с БД. Код ошибки: ' . $e->getMessage();
            } finally {
                self::$connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            }
        }

        # Фиксированный SELECT-запрос к БД
        public static function querySelect2()
        {
            $query = "SELECT * FROM `category`";
            return $query;
        }


    }
